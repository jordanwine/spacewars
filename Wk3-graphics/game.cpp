

// game.cpp

#include "game.h"

// Constructor
Game::Game()
{
	input = new Input();
	paused = false;			// game is not paused
	graphics = NULL;		
	initialized = false;
}

// Destructor
Game::~Game()
{
	deleteAll(); // free all reserved memory
	ShowCursor(true);		// show the cursor
}

// MessageHandler
LRESULT Game::messageHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	if( initialized )
	{
		switch ( msg ) 
		{
			case WM_DESTROY:
				PostQuitMessage(0);  // tell Windows OS to kill this program
				return 0;
			case WM_KEYDOWN: case WM_SYSKEYDOWN:  // key down
				input->keyDown(wParam);
				return 0;
			case WM_KEYUP: case WM_SYSKEYUP:  // key up
				input->keyUp(wParam);
				return 0;
			case WM_CHAR: // character entered
				input->keyIn(wParam);
				return 0;
			case WM_MOUSEMOVE: // mouse moved
				input->mouseIn( lParam );
				return 0;
			case WM_INPUT: // raw mouse data in
				input->mouseRawIn(lParam);
				return 0;
			case WM_LBUTTONDOWN:  // left mouse button down
				input->setMouseLButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_LBUTTONUP:  // left mouse button up
				input->setMouseLButton(false);
				input->mouseIn(lParam);
				return 0;
			case WM_MBUTTONDOWN:  // middle mouse button down
				input->setMouseMButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_MBUTTONUP:  // middle mouse button up
				input->setMouseMButton(false);
				input->mouseIn(lParam);
				return 0;
			case WM_RBUTTONDOWN:  // right mouse button down
				input->setMouseRButton(true);
				input->mouseIn(lParam);
				return 0;
			case WM_RBUTTONUP:  // right mouse button up
				input->setMouseRButton(false);
				input->mouseIn(lParam);
				return 0;
			case WM_DEVICECHANGE: // check for controllers
				input->checkControllers();
				return 0;
		}
	}
	return DefWindowProc( hwnd, msg, wParam, lParam); // let Windows OS handle it
}

// initialize function
void Game::initialize( HWND hw )
{
	hwnd = hw; // save windows handle

	// initialize graphics
	graphics = new Graphics();
	graphics->initialize(hwnd, GAME_WIDTH, GAME_HEIGHT, FULLSCREEN );

	// initialize input, do not capture mouse
	input->initialize( hwnd, false );
	
	// set up high resolution timer
	if( QueryPerformanceFrequency( &timerFreq ) == false )
	{
		throw( GameError( gameErrorNS::FATAL_ERROR, "Error initializing high res timer"));
	}
	QueryPerformanceCounter( &timeStart ); // get starting time

	// initialization completed!
	initialized = true;
}

void Game::handleLostGraphicsDevice()
{
	// test for and handle lost device
	hr = graphics->getDeviceState();
	if( FAILED(hr) )  // if the graphics device is not in a valid state
	{
		if( hr == D3DERR_DEVICELOST ) // if the device is lost and not available for reset
		{
			Sleep( 100 ); // yield cpu time for 100 milliseconds
			return;
		}
		else if( hr == D3DERR_DEVICENOTRESET ) // if the device was lost but now is available for reset
		{
			releaseAll();
			hr = graphics->reset(); // attempt to reset graphics device
			if( FAILED(hr) ) return; // if reset failed, return
			resetAll();
		}
		else {
			return;  // other device error
		}
	}
}
// render game items
void Game::renderGame()
{
	if( SUCCEEDED( graphics->beginScene() ) )
	{
		render(); // call render in derived class (SpaceWar)
		
		graphics->endScene();
	}
	handleLostGraphicsDevice();

	graphics->showBackBuffer(); // display the backbuffer to the screen
}

// run function, called repeatly in winmain
void Game::run( HWND hwnd )
{
	if ( graphics == NULL ) // if graphics not initialized
		return;
	
	// calculate elapsed time of last frame, save in framTime
	QueryPerformanceCounter( &timeEnd );
	frameTime = (float)(timeEnd.QuadPart - timeStart.QuadPart ) / 
				(float)timerFreq.QuadPart;

	//power saving code
	if( frameTime < MIN_FRAME_TIME )
	{
		sleepTime = (DWORD)((MIN_FRAME_TIME - frameTime)*1000);
		timeBeginPeriod(1);
		Sleep( sleepTime );
		timeEndPeriod(1);
		return;
	}

	if( frameTime > 0.0 )
	{
		fps = (fps*0.99f) + (0.01f/frameTime);
	}

	if( frameTime > MAX_FRAME_TIME )
	{
		frameTime = MAX_FRAME_TIME; 
	}

	timeStart = timeEnd;

	if( !paused ) //if not paused
	{
		update();	//update all game items
		ai();		//artifical intelligence
		collisions(); // handle collisions
		input->vibrateControllers( frameTime );	//handle controller vibration
	}
	renderGame();				//draw all game items
	input->readControllers();	//read state of controllers

	//clear input
	input->clear( inputNS::KEYS_PRESSED );
}

void Game::releaseAll()
{ }

void Game::resetAll()
{ }

void Game::deleteAll()
{
	releaseAll();
	SAFE_DELETE(graphics);
	SAFE_DELETE(input);

}
