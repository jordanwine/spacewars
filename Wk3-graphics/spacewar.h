#ifndef _SPACEWAR_H
#define _SPACEWAR_H
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "planet.h"
#include "ship.h"

class Spacewar : public Game
{
private:
	//variables
	TextureManager nebulaTexture; 
	TextureManager planetTexture;
	TextureManager shipTexture;

	Image nebula;
	Planet planet;
	Ship ship;

public:
	Spacewar();
	virtual ~Spacewar();

	void initialize( HWND hwnd );
	void update();
	void ai();
	void collisions();
	void render();

	void releseALL();
	void resetALL();

};
#endif