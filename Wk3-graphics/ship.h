#ifndef _SHIP_H_
#define _SHIP_H_
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace shipNS
{
	
	const int WIDTH = 32;
	const int HEIGHT = 32;
	const int X = GAME_WIDTH/2 - WIDTH/2;
	const int Y = GAME_HEIGHT/2 - HEIGHT/2;

	const float SPEED = 100.0f;
	const float MASS = 300.0f;
	const float ROTATION_RATE = (float)PI/4;
 
	const int TEXTURE_COLS = 2;
	const int SHIP_START_FRAME = 0;	//starting frame
	const int SHIP_END_FRAME = 3;	//ending frame
	const float SHIP_ANIMATION_DELAY = 0.9f;	// delay between frames

}

class Ship : public Entity
{
public:
	Ship();

	void update( float frameTime );
};

#endif
