#include "Ship.h"

Ship::Ship() : Entity()
{
	spriteData.width = shipNS::WIDTH;
	spriteData.height = shipNS::HEIGHT;
	spriteData.x = shipNS::X;
	spriteData.y = shipNS::Y;
	spriteData.rect.right = shipNS::WIDTH;
	spriteData.rect.bottom = shipNS::HEIGHT;

	frameDelay = shipNS::SHIP_ANIMATION_DELAY;
	startFrame = shipNS::SHIP_START_FRAME;
	endFrame = shipNS::SHIP_END_FRAME;
	currentFrame = startFrame;

	collisionType = entityNS::CIRCLE;
	radius = shipNS::WIDTH/2.0f;

	velocity.x = 0;
	velocity.y = 0;
}

void Ship::update ( float frameTime )
{
	Entity::update( frameTime );

	spriteData.angle += frameTime * shipNS::ROTATION_RATE;
	spriteData.x += frameTime * velocity.x;
	spriteData.y += frameTime * velocity.y;

	if( spriteData.x > (GAME_WIDTH - shipNS::WIDTH * getScale() ))
	{
		spriteData.x = GAME_WIDTH - shipNS::WIDTH * getScale();
		velocity.x = -velocity.x;
	}
	else if( spriteData.x < 0 )
	{
		spriteData.x = 0;
		velocity.x = -velocity.x;
	}
	if( spriteData.y > (GAME_HEIGHT - shipNS::HEIGHT * getScale() ))
	{
		spriteData.y = GAME_HEIGHT - shipNS::WIDTH * getScale();
		velocity.y = -velocity.y;
	}
	else if( spriteData.y < 0 )
	{
		spriteData.y = 0;
		velocity.y = -velocity.y;
	}
}