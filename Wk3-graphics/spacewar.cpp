#include "spacewar.h"
#include "ship.h"

Spacewar::Spacewar()
{}

Spacewar::~Spacewar()
{
	releseALL();
}

void Spacewar::initialize( HWND hwnd )
{
	Game::initialize( hwnd );

	if ( !nebulaTexture.initialize( graphics, NEBULA_IMAGE ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing nebula texture" ) );
	}
	
	if ( !planetTexture.initialize( graphics, PLANET_IMAGE ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing planet texture" ) );
	}

		if ( !shipTexture.initialize( graphics, SHIP_IMAGE ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing ship texture" ) );
	}

	if( !nebula.initialize( graphics, 0, 0, 0, &nebulaTexture ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing nebula image" ) );
	}

	if( !planet.initialize( this, 0, 0, 0, &planetTexture ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing planet" ) );
	}

	//place planet center screen
	planet.setX( GAME_WIDTH*0.5f - planet.getWidth()*0.5f );
	planet.setY( GAME_HEIGHT*0.5f - planet.getHeight()*0.5f );
		if( !ship.initialize( this, shipNS::WIDTH, shipNS::HEIGHT, shipNS::TEXTURE_COLS, &shipTexture ) )
	{
		throw ( GameError( gameErrorNS::FATAL_ERROR, "Error initializing ship image" ) );
	}
		ship.setX( GAME_WIDTH/4 );
		ship.setY( GAME_HEIGHT/4 );

		ship.setFrames( shipNS::SHIP_START_FRAME, shipNS::SHIP_END_FRAME );
		ship.setCurrentFrame( shipNS::SHIP_START_FRAME );
		ship.setFrameDelay( shipNS::SHIP_ANIMATION_DELAY );

		ship.setVelocity( VECTOR2( 0,-shipNS::SPEED ) );

	return;
}

void Spacewar::update()
{
	ship.gravityForce( &planet, frameTime );
	
	ship.update( frameTime );
	planet.update( frameTime );

	/* rotate
	ship.setDegrees( ship.getDegrees() + frameTime * 180.0f );

	scale
	ship.setScale( ship.getScale() - frameTime * 0.2f );

	move
	ship.setX( ship.getX() + frameTime * 1000.0f );

	if( ship.getX() > GAME_WIDTH )
	{
		ship.setX( (float)-ship.getWidth() );
		ship.setScale( 1.5f );
	}*/

	//if( input->isKeyDown( SHIP_RIGHT_KEY) )
	//{
	//	if( ship.getX() < ( GAME_WIDTH - ship.getWidth() ) ) 
	//	{
	//		ship.setX( ship.getX() + frameTime * shipNS::SPEED );
	//	}
	//}
	//if( input->isKeyDown( SHIP_LEFT_KEY) )
	//{
	//		if( ship.getX() > 0 ) 
	//	{
	//		ship.setX( ship.getX() - frameTime * shipNS::SPEED );
	//	}
	//}
	//if( input->isKeyDown( SHIP_DOWN_KEY) )
	//{
	//	ship.setY( ship.getY() + frameTime * shipNS::SPEED );
	//}
	//if( input->isKeyDown( SHIP_UP_KEY) )
	//{
	//	ship.setY( ship.getY() - frameTime * shipNS::SPEED );
	//}
}

void Spacewar::ai()
{}

void Spacewar::collisions()
{}

void Spacewar::render()
{
	graphics->spriteBegin();
	// call draw functions here!

	nebula.draw();
	planet.draw();
	ship.draw();

	// call draw functions here!
	graphics->spriteEnd();
}

void Spacewar::releseALL()
{
	shipTexture.onLostDevice();
	planetTexture.onLostDevice();
	nebulaTexture.onLostDevice();
	Game::releaseAll();

}

void Spacewar::resetALL()
{
	planetTexture.onResetDevice();
	nebulaTexture.onResetDevice();
	shipTexture.onResetDevice();
	Game::resetAll();
}